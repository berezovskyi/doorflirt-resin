# README #

### What is this repository for? ###

This is a complementary source code repository for the [blog post](https://berezovskiy.me/2015/05/c-cpp-based-projects-on-resin-io/)
on [my blog](https://berezovskiy.me/).

It contains a partial directory structure that was ported to [resin.io](https://resin.io).
 
### How do I get set up? ###

1. Sign up for *resin.io*
2. Create a project for the Raspberry Pi & set up the micro SD card
3. Define the `SPIDEV` environment variable
4. Wire up the camera sensor
5. Enable the port forwarding in resin.io dashboard.
6. Deploy the code to *resin.io*
  