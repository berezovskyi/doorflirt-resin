#!/usr/bin/env python

from SimpleHTTPServer import SimpleHTTPRequestHandler
import SocketServer
import subprocess
import urlparse

PORT = 8080

class AsciiHandler(SimpleHTTPRequestHandler):

	def do_GET(self):
		parsedParams = urlparse.urlparse(self.path)

		# request is either for a file to be served up or our page
		if parsedParams.path == "/":

			self.send_response(200)
			self.send_header('Content-Type', 'text/html')
			self.end_headers()
			self.wfile.write("<!DOCTYPE html>");
			self.wfile.write("<html><head></head><body><pre>");

			proc = subprocess.Popen(['./show_ascii'], stdout=subprocess.PIPE)
			while True:
				line = proc.stdout.readline()
				if line != '':
					self.wfile.write(line)
				else:
					break

			self.wfile.write("</pre>")
			self.wfile.write("<br><img src='image.png'>")
			self.wfile.write("</body></html>")
			self.wfile.close()
		else:
			# old-style class
			SimpleHTTPRequestHandler.do_GET(self)

httpd = SocketServer.TCPServer(("", PORT), AsciiHandler)

print "serving at port", PORT
httpd.serve_forever()
