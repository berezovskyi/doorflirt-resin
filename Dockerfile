FROM resin/rpi-raspbian:wheezy

RUN apt-get update && apt-get install -y jp2a make imagemagick gcc-4.7 g++-4.7 python2.7 
# RUN apt-get install -y cmake libcurl4-openssl-dev libopencv-dev

COPY . /app

WORKDIR /app/sample/lepton-pgm
RUN CC=gcc-4.7 make

CMD modprobe spi-dev
CMD python2.7 server.py